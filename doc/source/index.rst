.. Python references for advance usage documentation master file, created by
   sphinx-quickstart on Fri Feb 24 00:04:48 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Python reference guide
======================

Contents:

.. toctree::
   :maxdepth: 2
   :numbered:
   
   pyref/objects
   pyref/exception



Indices and tables
==================

* :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

